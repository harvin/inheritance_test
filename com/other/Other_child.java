package com.other;
import com.harvin.*;

public class Other_child extends Parent{
	private   void private_method() {
		//这里会报错。因为private修饰的成员，其他类均不可访问
		super.private_method();
	}
	protected void protected_method() {
		//这里会报错。不在同一个包内，但是是parent的子类，所以可以访问
		super.protected_method();
	}
	void default_method(){
		//这里会报错。不在同一个包内，不可以访问
		super.default_method();
	}
	public	  void public_method(){
		super.public_method();
	}
}
