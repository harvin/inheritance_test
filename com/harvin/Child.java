package com.harvin;

public class Child{
	private   void private_method() {
		//这里会报错。因为private修饰的成员，其他类均不可访问
		new Parent().private_method();
	}
	protected void protected_method() {
		//同一个包内，不过是不是子类，都可以访问到protected成员
		new Parent().protected_method();
	}
	void default_method(){
		//同一个包内，也可以访问
		new Parent().default_method();
	}
	public	  void public_method(){
		new Parent().public_method();
	}
}
